
module.exports = {
    plugins: [{
        tailwindcss: { config: './tailwindcss-config.js' },
    },
        require('tailwindcss'),
        require('autoprefixer'),
        require('cssnano')({
            preset: 'default',
        }),
    ],
    }
