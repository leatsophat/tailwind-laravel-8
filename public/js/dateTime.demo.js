function dataTimeDemo() {
    var x   	=   new Date()
    var ampm    =   x.getHours( )   >= 12 ? ' PM' : ' AM';
    var x1      =   x.getDate()     + 1+ "/" + x.getMonth() + "/" + x.getFullYear();
    x1          =   x1 + " - "      +  x.getHours( )+ ":" +  x.getMinutes() + ":" +  x.getSeconds() + "" + ampm;
    document.getElementById('datetime').innerHTML = x1;
    displayDemo();
}
function displayDemo(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('dataTimeDemo()',refresh)
}
displayDemo()
