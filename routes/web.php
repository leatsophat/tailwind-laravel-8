<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Welcome
Route::get('/', function () {
    return view('choose');
});








// temeplate A
Route::get('/templateA',function(){return view('componentsA.Dashboard',['title'=>'Dashboard temeplate A']); });

Route::get('/templateA/lineChart',function(){return view('componentsA.lineChart',['title'=>'lineChart temeplate A']); });













// Template B
Route::get('/templateB', function () {
    return view('componentsB.DashboardB',['title'=>'Dashboard temeplate B']);
});
