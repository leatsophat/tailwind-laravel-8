@extends('masterB')
@section('content')



{{-- Content  --}}


        {{-- Breadcrumbs and DateTime --}}
        <div class="grid grid-cols-2 gap-2 px-3 lg:grid-cols-2 xl:grid-cols-2 text-sm">
            <div class="containerblock">
                <ol class="list-reset py-4 rounded flex bg-grey-light text-grey">
                    <li class="px-1 pr-2"><a href="/" class="no-underline textBlue">SmartERP</a></li>
                    <li class=" text-gray-800 dark:text-gray-300">/</li>
                    <li class="px-2"><a href="#" class="no-underline text-indigo text-gray-800 dark:text-gray-300">Dashboard</a></li>
                    <li class=" text-gray-800 dark:text-gray-300">/</li>
                    <li class="px-2 text-gray-500">Data</li>
                </ol>
            </div>
            <div class="p-4 pr-2">
                <span class="text-gray-800 dark:text-gray-300 block float-right" id='datetime'></span>
            </div>
        </div>
        {{-- Breadcrumbs and DateTime --}}


        {{-- title --}}
        <div class="grid grid-cols-1 px-3 lg:grid-cols-2">
            <h4 class="inline-block align-top text-xl font-semibold text-gray-800 dark:text-gray-300"><i class="fr chart-pie-alt t-sm textBlue"></i> Dashboard <span class="text-gray-500 font-normal">Data</span></h4>
        </div>
        {{-- title --}}



    {{-- State cards  --}}
    <div class="grid grid-cols-1 gap-4 p-4 lg:grid-cols-2 xl:grid-cols-4">
        {{-- Value card  --}}
        <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-darker">
            <div>
                <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light"> Value </h6>
                <span class="text-xl font-semibold">$30,000</span>
                <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
            </div>
            <div>
                <span> <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i> </span>
            </div>
        </div>
        {{-- Value card  --}}
        <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-darker">
            <div>
                <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light"> Value </h6>
                <span class="text-xl font-semibold">$30,000</span>
                <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
            </div>
            <div>
                <span> <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i> </span>
            </div>
        </div>
        {{-- Value card  --}}
        <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-darker">
            <div>
                <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light"> Value </h6>
                <span class="text-xl font-semibold">$30,000</span>
                <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
            </div>
            <div>
                <span> <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i> </span>
            </div>
        </div>
        {{-- Value card  --}}
        <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-darker">
            <div>
                <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light"> Value </h6>
                <span class="text-xl font-semibold">$30,000</span>
                <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
            </div>
            <div>
                <span> <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i> </span>
            </div>
        </div>

    </div>

    
    {{-- Two grid columns  --}}
    <div class="grid grid-cols-1 p-4 space-y-8 lg:gap-4 lg:space-y-0 lg:grid-cols-3">
        {{-- Active users chart  --}}
        <div class="col-span-2 bg-white rounded-md dark:bg-darker">
            {{-- Card header  --}}
            <div class="p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Active users right now</h4>
            </div>
            <p class="p-4">
                <span class="text-2xl font-medium text-gray-500 dark:text-light" id="usersCount">0</span>
                <span class="text-sm font-medium text-gray-500 dark:text-primary">Users</span>
            </p>
            {{-- Chart  --}}
            <div class="relative p-4">
                <canvas id="activeUsersChart"></canvas>
            </div>
        </div>

        {{-- Line chart card  --}}
        <div class="col-span-1 bg-white rounded-md dark:bg-darker" x-data="{ isOn: false }">
            {{-- Card header  --}}
            <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Line Chart</h4>
                <div class="flex items-center">
                    <button class="relative focus:outline-none" x-cloak
                        @click="isOn = !isOn; $parent.updateLineChart()">
                        <div
                            class="w-12 h-6 transition rounded-full outline-none bg-primary-100 dark:bg-primary-darker">
                        </div>
                        <div class="absolute top-0 left-0 inline-flex items-center justify-center w-6 h-6 transition-all duration-200 ease-in-out transform scale-110 rounded-full shadow-sm"
                            :class="{ 'translate-x-0  bg-white dark:bg-primary-100': !isOn, 'translate-x-6 bg-primary-light dark:bg-primary': isOn }">
                        </div>
                    </button>
                </div>
            </div>
            {{-- Chart  --}}
            <div class="relative p-4 h-72">
                <canvas id="lineChart"></canvas>
            </div>
        </div>
    </div>

    
    {{-- Charts  --}}
    <div class="grid grid-cols-1 px-4 space-y-8 lg:gap-4 lg:space-y-0 lg:grid-cols-3">
        {{-- Bar chart card  --}}
        <div class="col-span-2 bg-white rounded-md dark:bg-darker" x-data="{ isOn: false }">
            {{-- Card header  --}}
            <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Bar Chart</h4>
                <div class="flex items-center space-x-2">
                    <span class="text-sm text-gray-500 dark:text-light">Last year</span>
                    <button class="relative focus:outline-none" x-cloak
                        @click="isOn = !isOn; $parent.updateBarChart(isOn)">
                        <div class="w-12 h-6 transition rounded-full outline-none bg-primary-100 dark:bg-primary-darker">
                        </div>
                        <div class="absolute top-0 left-0 inline-flex items-center justify-center w-6 h-6 transition-all duration-200 ease-in-out transform scale-110 rounded-full shadow-sm"
                            :class="{ 'translate-x-0  bg-white dark:bg-primary-100': !isOn, 'translate-x-6 bg-primary-light dark:bg-primary': isOn }">
                        </div>
                    </button>
                </div>
            </div>
            {{-- Chart  --}}
            <div class="relative p-4 h-72">
                <canvas id="barChart"></canvas>
            </div>
        </div>

        {{-- Doughnut chart card  --}}
        <div class="bg-white rounded-md dark:bg-darker" x-data="{ isOn: false }">
            {{-- Card header  --}}
            <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Doughnut Chart</h4>
                <div class="flex items-center">
                    <button class="relative focus:outline-none" x-cloak
                        @click="isOn = !isOn; $parent.updateDoughnutChart(isOn)">
                        <div
                            class="w-12 h-6 transition rounded-full outline-none bg-primary-100 dark:bg-primary-darker">
                        </div>
                        <div class="absolute top-0 left-0 inline-flex items-center justify-center w-6 h-6 transition-all duration-200 ease-in-out transform scale-110 rounded-full shadow-sm"
                            :class="{ 'translate-x-0  bg-white dark:bg-primary-100': !isOn, 'translate-x-6 bg-primary-light dark:bg-primary': isOn }">
                        </div>
                    </button>
                </div>
            </div>
            {{-- Chart  --}}
            <div class="relative p-4 h-72">
                <canvas id="doughnutChart"></canvas>
            </div>
        </div>
    </div>

</div>
{{-- card  --}}
<script>
    const setup = () => {
      const getTheme = () => {
        if (window.localStorage.getItem('dark')) {
          return JSON.parse(window.localStorage.getItem('dark'))
        }

        return !!window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
      }

      const setTheme = (value) => {
        window.localStorage.setItem('dark', value)
      }

      const getColor = () => {
        if (window.localStorage.getItem('color')) {
          return window.localStorage.getItem('color')
        }
        return 'cyan'
      }

      const setColors = (color) => {
        const root = document.documentElement
        root.style.setProperty('--color-primary', `var(--color-${color})`)
        root.style.setProperty('--color-primary-50', `var(--color-${color}-50)`)
        root.style.setProperty('--color-primary-100', `var(--color-${color}-100)`)
        root.style.setProperty('--color-primary-light', `var(--color-${color}-light)`)
        root.style.setProperty('--color-primary-lighter', `var(--color-${color}-lighter)`)
        root.style.setProperty('--color-primary-dark', `var(--color-${color}-dark)`)
        root.style.setProperty('--color-primary-darker', `var(--color-${color}-darker)`)
        this.selectedColor = color
        window.localStorage.setItem('color', color)
        //
      }

      const updateBarChart = (on) => {
        const data = {
          data: randomData(),
          backgroundColor: 'rgb(207, 250, 254)',
        }
        if (on) {
          barChart.data.datasets.push(data)
          barChart.update()
        } else {
          barChart.data.datasets.splice(1)
          barChart.update()
        }
      }

      const updateDoughnutChart = (on) => {
        const data = random()
        const color = 'rgb(207, 250, 254)'
        if (on) {
          doughnutChart.data.labels.unshift('Seb')
          doughnutChart.data.datasets[0].data.unshift(data)
          doughnutChart.data.datasets[0].backgroundColor.unshift(color)
          doughnutChart.update()
        } else {
          doughnutChart.data.labels.splice(0, 1)
          doughnutChart.data.datasets[0].data.splice(0, 1)
          doughnutChart.data.datasets[0].backgroundColor.splice(0, 1)
          doughnutChart.update()
        }
      }

      const updateLineChart = () => {
        lineChart.data.datasets[0].data.reverse()
        lineChart.update()
      }

      return {
        loading: true,
        isDark: getTheme(),
        toggleTheme() {
          this.isDark = !this.isDark
          setTheme(this.isDark)
        },
        setLightTheme() {
          this.isDark = false
          setTheme(this.isDark)
        },
        setDarkTheme() {
          this.isDark = true
          setTheme(this.isDark)
        },
        color: getColor(),
        selectedColor: 'cyan',
        setColors,
        toggleSidbarMenu() {
          this.isSidebarOpen = !this.isSidebarOpen
        },
        isSettingsPanelOpen: false,
        openSettingsPanel() {
          this.isSettingsPanelOpen = true
          this.$nextTick(() => {
            this.$refs.settingsPanel.focus()
          })
        },
        isNotificationsPanelOpen: false,
        openNotificationsPanel() {
          this.isNotificationsPanelOpen = true
          this.$nextTick(() => {
            this.$refs.notificationsPanel.focus()
          })
        },
        isSearchPanelOpen: false,
        openSearchPanel() {
          this.isSearchPanelOpen = true
          this.$nextTick(() => {
            this.$refs.searchInput.focus()
          })
        },
        isMobileSubMenuOpen: false,
        openMobileSubMenu() {
          this.isMobileSubMenuOpen = true
          this.$nextTick(() => {
            this.$refs.mobileSubMenu.focus()
          })
        },
        isMobileMainMenuOpen: false,
        openMobileMainMenu() {
          this.isMobileMainMenuOpen = true
          this.$nextTick(() => {
            this.$refs.mobileMainMenu.focus()
          })
        },
        updateBarChart,
        updateDoughnutChart,
        updateLineChart,
      }
    }
  </script>

@endsection
