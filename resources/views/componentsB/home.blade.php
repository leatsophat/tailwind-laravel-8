@extends('masterA')
@section('content')
<div class="w-full mx-auto px-2">
    {{-- Card  --}}
     <div id='recipients' class="p-8 mt-6 shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

        <table id="example" class="display min-w-full divide-y divide-gray-200" cellspacing="0" width="100%">
            <thead class="bg-gray-50">
                <tr>
                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Title</th>
                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Status</th>
                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Role</th>
                    <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="flex-shrink-0 h-10 w-10">
                              <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt="">
                            </div>
                            <div class="ml-4">
                              <div class="text-sm font-medium text-gray-900"> Jane Cooper </div>
                              <div class="text-sm text-gray-500"> jane.cooper@example.com </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-sm text-gray-900">Regional Paradigm Technician</div>
                        <div class="text-sm text-gray-500">Optimization</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                            Active
                        </span>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        Admin
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                    </td>
                </tr>
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="flex-shrink-0 h-10 w-10">
                              <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt="">
                            </div>
                            <div class="ml-4">
                              <div class="text-sm font-medium text-gray-900">
                                Jane Cooper
                              </div>
                              <div class="text-sm text-gray-500">
                                jane.cooper@example.com
                              </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-sm text-gray-900">Regional Paradigm Technician</div>
                        <div class="text-sm text-gray-500">Optimization</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                            Active
                        </span>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        Admin
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                    </td>
                </tr>
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="flex-shrink-0 h-10 w-10">
                              <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt="">
                            </div>
                            <div class="ml-4">
                              <div class="text-sm font-medium text-gray-900">
                                Jane Cooper
                              </div>
                              <div class="text-sm text-gray-500">
                                jane.cooper@example.com
                              </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-sm text-gray-900">Regional Paradigm Technician</div>
                        <div class="text-sm text-gray-500">Optimization</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                            Active
                        </span>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        Admin
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                    </td>
                </tr>
            </tbody>
        </table>


    </div>
    {{-- /Card  --}}


</div>
</html>



<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            // button
            dom: 'Bfrtip',
            buttons: [
            { "extend": 'pdf', "text":'Export',"className": 'mr-1 bg-blue-500 hover:bg-blue-700 text-white font-bold p-2 rounded' },
            { "extend": 'copy', "text":'Copy',"className": 'mr-1 bg-gray-500 hover:bg-blue-700 text-white font-bold p-2 rounded' },
            { "extend": 'csv', "text":'CSV',"className": 'mr-1 bg-blue-500 hover:bg-blue-700 text-white font-bold p-2 rounded' },
            { "extend": 'excel', "text":'Excel',"className": 'mr-1 bg-green-500 hover:bg-blue-700 text-white font-bold p-2 rounded' },
            { "extend": 'pdf', "text":'PDF',"className": 'mr-1 bg-red-500 hover:bg-red-700 text-white font-bold p-2 rounded' }
            ],
            // Select
            select: true,
            dom: 'Blfrtip',
            lengthMenu: [
                [1,10, 25, 50, -1],
                ['1','10', '25', '50', 'all']
            ],
        } );
    } );
</script>
@endsection


