@extends('masterA')
@section('content')

    <div class="mt-2">

        {{-- Breadcrumbs and DateTime --}}
        <div class="grid grid-cols-2 gap-2 px-3 lg:grid-cols-2 xl:grid-cols-2 text-sm">
            <div class="containerblock">
                <ol class="list-reset py-4 rounded flex bg-grey-light text-grey">
                    <li class="px-1 pr-2"><a href="/" class="no-underline textBlue">SmartERP</a></li>
                    <li class=" text-gray-800 dark:text-gray-300">/</li>
                    <li class="px-2"><a href="#" class="no-underline text-indigo text-gray-800 dark:text-gray-300">Dashboard</a></li>
                </ol>
            </div>
            <div class="p-4 pr-2">
                <span class="text-gray-800 dark:text-gray-300 block float-right" id='datetime'></span>
            </div>
        </div>
        {{-- Breadcrumbs and DateTime --}}


        {{-- title --}}
        <div class="grid grid-cols-1 px-3 lg:grid-cols-2">
            <h4 class="inline-block align-top text-xl font-semibold text-gray-800 dark:text-gray-300"><i class="fr chart-pie-alt t-sm textBlue"></i> Dash<span class="text-gray-500 font-normal">board</span></h4>
        </div>
        {{-- title --}}

        <div class="grid grid-cols-1 gap-4 p-4 lg:grid-cols-2 xl:grid-cols-4">
            {{-- Value card  --}}
            <div class="flex items-center justify-between p-4 bg-gradient-to-r from-green-400 to-blue-500 rounded-md dark:bg-gray-800 dark:border-gray-200">
                <div>
                    <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light" > Value </h6>
                    <span class="text-xl font-semibold text-gray-800 dark:text-gray-300">$30,000</span>
                    <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
                </div>
                <div>
                    <span>
                        <i class="fl usd-circle t-3x text-blue-100 dark:text-blue-100"></i>
                    </span>
                </div>
            </div>
            {{-- Value card  --}}


            {{-- Value card  --}}
            <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200">
                <div>
                    <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light" > Value </h6>
                    <span class="text-xl font-semibold text-gray-800 dark:text-gray-300">$30,000</span>
                    <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
                </div>
                <div>
                    <span>
                        <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i>
                    </span>
                </div>
            </div>
            {{-- Value card  --}}


            {{-- Value card  --}}
            <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200">
                <div>
                    <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light" > Value </h6>
                    <span class="text-xl font-semibold text-gray-800 dark:text-gray-300">$30,000</span>
                    <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
                </div>
                <div>
                    <span>
                        <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i>
                    </span>
                </div>
            </div>
            {{-- Value card  --}}


            {{-- Value card  --}}
            <div class="flex items-center justify-between p-4 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200">
                <div>
                    <h6 class="text-xs font-medium leading-none tracking-wider text-gray-500 uppercase dark:text-primary-light" > Value </h6>
                    <span class="text-xl font-semibold text-gray-800 dark:text-gray-300">$30,000</span>
                    <span class="inline-block px-2 py-px ml-2 text-xs text-green-500 bg-green-100 rounded-md"> +4.4% </span>
                </div>
                <div>
                    <span>
                        <i class="fl usd-circle t-3x text-blue-500 dark:text-blue-100"></i>
                    </span>
                </div>
            </div>
            {{-- Value card  --}}

        </div>


        {{-- Charts  --}}
        <div class="grid grid-cols-1 px-4 space-y-8 lg:gap-4 lg:space-y-0 lg:grid-cols-3">

            {{-- Active users chart  --}}
            <div class="col-span-2 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200">
                {{-- Card header  --}}
                <div class="p-4 border-b dark:border-primary">
                    <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Active users right now</h4>
                </div>
                <p class="p-4">
                    <span class="text-2xl font-medium text-gray-500 dark:text-light" id="usersCount">0</span>
                    <span class="text-sm font-medium text-gray-500 dark:text-primary">Users</span>
                </p>
                {{-- Chart  --}}
                <div class="relative p-4">
                    <canvas id="activeUsersChart"></canvas>
                </div>
            </div>

            {{-- Doughnut chart card  --}}
            <div class="bg-white rounded-md dark:bg-gray-800 dark:border-gray-200" x-data="{ isOn: false }">
                {{-- Card header  --}}
                <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Doughnut Chart</h4>
            </div>


            {{-- Chart  --}}
            <div class="relative p-4 h-72">
                <canvas id="doughnutChart"></canvas>
            </div>
        </div>
    </div>




    {{-- Two grid columns  --}}
    <div class="grid grid-cols-1 p-4 space-y-8 lg:gap-4 lg:space-y-0 lg:grid-cols-3">

        {{-- Bar chart card  --}}
        <div class="col-span-1 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200" x-data="{ isOn: false }">
            {{-- Card header  --}}
            <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Bar Chart</h4>
            </div>


            {{-- Chart  --}}
            <div class="relative p-4 h-72">
                <canvas id="barChart"></canvas>
            </div>
        </div>

        {{-- Line chart card  --}}
        <div class="col-span-2 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200" x-data="{ isOn: false }">
            {{-- Card header  --}}
            <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Line Chart</h4>
            </div>

            {{-- Chart  --}}
            <div class="relative p-4 h-72">
                <canvas id="lineChart"></canvas>
            </div>
        </div>
    </div>
</div>
















@endsection
