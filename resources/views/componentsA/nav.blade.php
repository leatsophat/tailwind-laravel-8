<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Stimulus and Tailwind</title>
    @include('layouts.header')
    <script src="https://unpkg.com/stimulus/dist/stimulus.umd.js"></script>
    <script>
      (() => {
        const application = Stimulus.Application.start()

        application.register("sidebar", class extends Stimulus.Controller {
            static get targets() {
                return [ "sidebarContainer", "icon", "link" ]
            }

            toggle() {
                if (this.sidebarContainerTarget.dataset.expanded === "1") {
                    this.collapse()
                } else {
                    this.expand()
                }
            }

            collapse() {
                this.sidebarContainerTarget.classList.remove("sm:w-1/5")
                this.sidebarContainerTarget.dataset.expanded = "0"
                this.iconTarget.innerHTML = `
                <!-- <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-10 text-gray-900" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg> -->
                `
                this.linkTargets.forEach(link => {
                link.classList.add("sr-only")
                })
            }

            expand() {
                this.sidebarContainerTarget.classList.add("sm:w-1/5")
                this.sidebarContainerTarget.dataset.expanded = "1"
                this.iconTarget.innerHTML = `
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-10 text-gray-900" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" />
                </svg>
                `
                this.linkTargets.forEach(link => {
                link.classList.remove("sr-only")
                })
            }
        })
      })()
    </script>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <style>
        .animate{
            transition: all 1s;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="flex">
            <aside class="sm:w-1/5 min-h-screen bg-gray-100" data-sidebar-target="sidebarContainer" data-expanded="1" data-controller="sidebar">
                <div class="sticky top-0 pt-0 w-full">
                    <div class="right-2 top-2 cursor-pointer h-17" data-action="click->sidebar#toggle" data-sidebar-target="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-10 text-gray-900" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h7" />
                        </svg>
                    </div>
                    <nav>
                        <ul class="flex flex-col overflow-hidden space-y-2 bg-blue-500">
                            <li class="h-17 w-55 m-auto bg-gray-100 w-full px-10">
                                <a class="flex items-center h-17"  data-action="click->sidebar#toggle" data-sidebar-target="icon">
                                    <img src="https://by3301files.storage.live.com/y4p-9o4SbWCydkF0WCFwETmhyKFNh9ZCntzrwjJED-2OrilFsmh4IWod-LNtpaMVJeA8ig-4jZOofDnFV_ACIjy-vULfmKCAahlpis-dBTkeqzg9PSv-ZzskdEPDlH1zZZEzZDseI5tTEQwxj5_9mOp1P8Fj2T20gwPOiqYiB1XTxB-6otJHHfZ9XBOmUTwDh3ige2Rcup91bJKDGF5zlRencTDMwEjvC7Z-l0rqpL0Y5E/logo_.png?psid=1&width=800&height=800&cropMode=center" alt="" class="" width="60px">
                                    <span data-sidebar-target="link">
                                        <img src="https://by3301files.storage.live.com/y4pskT4JqGR1QqtvRLV-dGvtN8fdEqkTOYkfHuIVqBNlftTgzisjJla7-qezzb5-nHZW3kpbBjIsX_LxdiSxW6-GNKyAhRlWpLERBsCs_Mif98AstIhrcHq40KwyZLxVo1Sgl9xejpLneNxYUsE17FYnSNtChvkCsJlqePgnv9WpWBF0s_f6LrfJgHuQMpFduU1c11LDvw8j1HZ8malmSe3TU3KdGq_ZMGhcg7GhctzUx4/logotext.png?psid=1&width=670&height=98" alt="" class="" width="200px">
                                    </span>
                                </a>

                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
                                    </svg>
                                    <span data-sidebar-target="link"> Sales </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 17h8m0 0V9m0 8l-8-8-4 4-6-6" />
                                    </svg>
                                    <span data-sidebar-target="link"> Finance </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                    <span data-sidebar-target="link"> Services </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 4H6a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-2m-4-1v8m0 0l3-3m-3 3L9 8m-5 5h2.586a1 1 0 01.707.293l2.414 2.414a1 1 0 00.707.293h3.172a1 1 0 00.707-.293l2.414-2.414a1 1 0 01.707-.293H20" />
                                      </svg>
                                    <span data-sidebar-target="link"> Inventory & MRP </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                      </svg>
                                    <span data-sidebar-target="link"> Human Resources </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 21h7a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v11m0 5l4.879-4.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242z" />
                                      </svg>
                                    <span data-sidebar-target="link"> eDocuments </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                                      </svg>
                                    <span data-sidebar-target="link"> eApprovals </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122" />
                                    </svg>
                                    <span data-sidebar-target="link"> Verify Customer </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                    <span data-sidebar-target="link"> Fleet </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                      </svg>
                                    <span data-sidebar-target="link"> SmartGPS </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z" />
                                    </svg>
                                    <span data-sidebar-target="link"> Set Roles </span>
                                </a>
                            </li>
                            <li class="text-gray-200 hover:text-gray-200 h-8">
                                <a href="#" class="flex items-center h-8">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-9 mr-1 inline-block" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
                                      </svg>
                                    <span data-sidebar-target="link"> Drawmap </span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <main class="sm:w-4/5 p-4 pt-12 px-8">
                <div class="space-y-4">
                    <!-- From https://climate.nasa.gov/evidence/ -->
                    <p>Earth's climate has changed throughout history. Just in the last 650,000 years there have been seven cycles of glacial advance and retreat, with the abrupt end of the last ice age about 11,700 years ago marking the beginning of the modern climate era — and of human civilization. Most of these climate changes are attributed to very small variations in Earth’s orbit that change the amount of solar energy our planet receives.</p>
                    <p>The current warming trend is of particular significance because most of it is extremely likely (greater than 95% probability) to be the result of human activity since the mid-20th century and proceeding at a rate that is unprecedented over millennia.</p>
                    <p>Earth-orbiting satellites and other technological advances have enabled scientists to see the big picture, collecting many different types of information about our planet and its climate on a global scale. This body of data, collected over many years, reveals the signals of a changing climate.</p>
                    <p>The heat-trapping nature of carbon dioxide and other gases was demonstrated in the mid-19th century. Their ability to affect the transfer of infrared energy through the atmosphere is the scientific basis of many instruments flown by NASA. There is no question that increased levels of greenhouse gases must cause Earth to warm in response.</p>
                    <p>Ice cores drawn from Greenland, Antarctica, and tropical mountain glaciers show that Earth’s climate responds to changes in greenhouse gas levels. Ancient evidence can also be found in tree rings, ocean sediments, coral reefs, and layers of sedimentary rocks. This ancient, or paleoclimate, evidence reveals that current warming is occurring roughly ten times faster than the average rate of ice-age-recovery warming. Carbon dioxide from human activity is increasing more than 250 times faster than it did from natural sources after the last Ice Age</p>
                    <p>The planet's average surface temperature has risen about 2.12 degrees Fahrenheit (1.18 degrees Celsius) since the late 19th century, a change driven largely by increased carbon dioxide emissions into the atmosphere and other human activities.4 Most of the warming occurred in the past 40 years, with the seven most recent years being the warmest. The years 2016 and 2020 are tied for the warmest year on record.</p>
                    <p>The ocean has absorbed much of this increased heat, with the top 100 meters (about 328 feet) of ocean showing warming of more than 0.6 degrees Fahrenheit (0.33 degrees Celsius) since 1969. Earth stores 90% of the extra energy in the ocean.</p>
                </div>
            </main>
        </div>
    </div>
</body>
</html>
