@extends('masterA')
@section('content')
<div class="mt-2 px-4">
    {{-- Breadcrumbs and DateTime --}}
    <div class="grid grid-cols-2 gap-2 px-3 lg:grid-cols-2 xl:grid-cols-2 text-sm">
        <div class="containerblock">
            <ol class="list-reset py-4 rounded flex bg-grey-light text-grey">
                <li class="px-1 pr-2"><a href="/" class="no-underline textBlue">SmartERP</a></li>
                <li class=" text-gray-800 dark:text-gray-300">/</li>
                <li class="px-2"><a href="#" class="no-underline text-indigo text-gray-800 dark:text-gray-300">Dashboard</a></li>
                <li class=" text-gray-800 dark:text-gray-300">/</li>
                <li class="px-2 text-gray-500">Data</li>
            </ol>
        </div>
        <div class="p-4 pr-2">
            <span class="text-gray-800 dark:text-gray-300 block float-right" id='datetime'></span>
        </div>
    </div>
    {{-- Breadcrumbs and DateTime --}}


    {{-- title --}}
    <div class="grid grid-cols-1 px-3 lg:grid-cols-2">
        <h4 class="inline-block align-top text-xl font-semibold text-gray-800 dark:text-gray-300"><i class="fr chart-pie-alt t-sm textBlue"></i> Dashboard <span class="text-gray-500 font-normal">Data</span></h4>
    </div>
    {{-- title --}}



    {{-- Card  --}}
     <div class="p-3 mt-6 shadow overflow-hidden border-b bg-gray-100 dark:bg-gray-800 border-gray-200 sm:rounded-lg ">
            <table id="example" class="display min-w-full text-gray-900 dark:text-gray-200" cellspacing="0" style="width:100%;">
                <thead>
                    <tr class="text-md font-semibold tracking-wide text-left text-gray-900 dark:text-gray-200 dark:hover:text-gray-700  uppercase border-b border-gray-600">
                        <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"> {{-- <span class="sr-only">Edit</span> --}} Action </th>
                        <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                        <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Title</th>
                        <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Status</th>
                        <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Role</th>

                    </tr>
                </thead>

                    <tbody>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4 dark:text-gray-200 dark:hover:text-gray-700">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10">
                                    <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt="">
                                    </div>
                                    <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Rachana JS </div>
                                    <div class="text-sm text-gray-500 dark:hover:text-gray-600"> js.rachnana@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Junior Programing Officer</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Lead</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10"> <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt=""> </div>
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Jane Cooper </div>
                                        <div class="text-sm text-gray-500 dark:hover:text-gray-600"> jane.cooper@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Regional Paradigm Technician</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Optimization</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10"> <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt=""> </div>
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Jane Cooper </div>
                                        <div class="text-sm text-gray-500 dark:hover:text-gray-600"> jane.cooper@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Regional Paradigm Technician</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Optimization</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10"> <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt=""> </div>
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Jane Cooper </div>
                                        <div class="text-sm text-gray-500 dark:hover:text-gray-600"> jane.cooper@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Regional Paradigm Technician</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Optimization</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10"> <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt=""> </div>
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Jane Cooper </div>
                                        <div class="text-sm text-gray-500 dark:hover:text-gray-600"> jane.cooper@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Regional Paradigm Technician</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Optimization</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10"> <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt=""> </div>
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Jane Cooper </div>
                                        <div class="text-sm text-gray-500 dark:hover:text-gray-600"> jane.cooper@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Regional Paradigm Technician</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Optimization</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>
                        <tr>
                            <td class="px-6 py-4">
                                <a href="#" class="text-indigo-600 dark:text-gray-300 dark:hover:text-indigo-600 hover:text-indigo-900"><t class="fl edit"></t><t class="fl eye"></t><t class="fl trash-alt"></t></a>
                            </td>
                            <td class="px-6 py-4">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0 h-10 w-10"> <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatar.jfif') }}" alt=""> </div>
                                    <div class="ml-4">
                                        <div class="text-sm font-medium text-gray-900 dark:text-gray-200 dark:hover:text-gray-700"> Jane Cooper </div>
                                        <div class="text-sm text-gray-500 dark:hover:text-gray-600"> jane.cooper@example.com </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4">
                                <div class="text-sm text-gray-900 dark:text-gray-200 dark:hover:text-gray-700">Regional Paradigm Technician</div>
                                <div class="text-sm text-gray-500 dark:hover:text-gray-600">Optimization</div>
                            </td>
                            <td class="px-6 py-4">
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"> Active </span>
                            </td>
                            <td class="px-6 py-4 text-gray-900 dark:text-gray-200"> Admin </td>

                        </tr>

                </tbody>

            </table>
    </div>
    {{-- /Card  --}}


</div>
</html>



<script>
    $(document).ready(function() {
        $('.dataTables_filter').addClass('focus:outline-none focus:ring focus:border-blue-300');
        $('#example').DataTable( {
            // button
            dom: 'Bfrtip',
            buttons: [
            { "extend": 'copy', "text":'Copy',"className": 'mr-1 border dark:text-white border-blue-500 uppercase p-2 rounded-sm' },
            { "extend": 'csv', "text":'CSV',"className": 'mr-1 border dark:text-white border-green-500 uppercase p-2 rounded-sm' },
            { "extend": 'excel', "text":'Excel',"className": 'mr-1 border dark:text-white border-green-500 uppercase p-2 rounded-sm' },
            { "extend": 'pdf', "text":'PDF',"className": 'mr-1 border dark:text-white border-red-500 uppercase p-2 rounded-sm' }
            ],
            // Select
            select: true,
            dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, -1],
                ['10', '25', '50', 'All']
            ],
            "scrollY": "400px",
            "scrollX": "1200px",

        } );
    } );
</script>

@endsection
