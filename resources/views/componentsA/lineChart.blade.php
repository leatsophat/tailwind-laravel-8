@extends('masterA')
@section('content')

<div class="mt-2">

    {{-- Breadcrumbs and DateTime --}}
    <div class="grid grid-cols-2 gap-2 px-3 lg:grid-cols-2 xl:grid-cols-2 text-sm">
        <div class="containerblock">
            <ol class="list-reset py-4 rounded flex bg-grey-light text-grey">
                <li class="px-1 pr-2"><a href="/" class="no-underline textBlue">SmartERP</a></li>
                <li class=" text-gray-800 dark:text-gray-300">/</li>
                <li class="px-2"><a href="#" class="no-underline text-indigo text-gray-800 dark:text-gray-300">Dashboard</a></li>
                <li class=" text-gray-800 dark:text-gray-300">/</li>
                <li class="px-2 text-gray-500">Data</li>
            </ol>
        </div>
        <div class="p-4 pr-2">
            <span class="text-gray-800 dark:text-gray-300 block float-right" id='datetime'></span>
        </div>
    </div>
    {{-- Breadcrumbs and DateTime --}}


    {{-- title --}}
    <div class="grid grid-cols-1 px-3 lg:grid-cols-2">
        <h4 class="inline-block align-top text-xl font-semibold text-gray-800 dark:text-gray-300"><i class="fr chart-line t-sm textBlue"></i> LineChart <span class="text-gray-500 font-normal">Data</span></h4>
    </div>
    {{-- title --}}
    <div class="grid grid-cols-2 gap-4 p-4 lg:grid-cols-2 xl:grid-cols-2">

        <div  x-data="app()" class="p-6 rounded-lg bg-white dark:bg-gray-800">
            <div class="md:flex md:justify-between md:items-center">
                <div>
                    <h2 class="text-xl text-gray-800 font-bold leading-tight dark:text-gray-300">Product Sales</h2>
                    <p class="mb-2 text-gray-600 text-sm dark:text-gray-400">Monthly Average</p>
                </div>

             {{-- Legends  --}}
            <div class="mb-4">
                <div class="flex items-center">
                    <div class="w-2 h-2 bgBlue mr-2 rounded-full dark:text-gray-200"></div>
                    <div class="text-sm text-gray-700 dark:text-gray-400">Sales</div>
                </div>
            </div>
            </div>


            <div class="line my-8 relative">
             {{-- Tooltip  --}}
            <template x-if="tooltipOpen == true">
                <div x-ref="tooltipContainer" class="p-0 m-0 z-10 lg rounded-lg absolute h-auto block dark:bg-gray-500" :style="`bottom: ${tooltipY}px; left: ${tooltipX}px`" >
                    <div class="xs rounded-lg bg-white dark:bg-gray-800 p-2">
                        <div class="flex items-center justify-between text-sm">
                            <div class=" dark:text-gray-200">Sales:</div>
                            <div class="font-bold ml-2">
                                <span x-html="tooltipContent" class=" dark:text-gray-200"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </template>

             {{-- Bar Chart  --}}
            <div class="flex -mx-2 items-end mb-2">
                <template x-for="data in chartData">

                    <div class="px-2 w-1/6">
                        <div :style="`height: ${data}px`" class="transition ease-in duration-200 bgBlue hover:bg-blue-400 relative" @mouseenter="showTooltip($event); tooltipOpen = true" @mouseleave="hideTooltip($event)" >
                            <div x-text="data" class="text-center absolute top-0 left-0 right-0 -mt-6 text-gray-800 dark:text-gray-200 text-sm"></div>
                        </div>
                    </div>

                </template>
            </div>

             {{-- Labels  --}}
            <div class="border-t border-gray-400 mx-auto" :style="`height: 1px; width: ${ 100 - 1/chartData.length*100 + 3}%`"></div>
                <div class="flex -mx-2 items-end">
                    <template x-for="data in labels">
                        <div class="px-2 w-1/6">
                            <div class="bg-red-600 relative">
                                <div class="text-center absolute top-0 left-0 right-0 h-2 -mt-px bg-gray-400 mx-auto" style="width: 1px"></div>
                                <div x-text="data" class="text-center absolute top-0 left-0 right-0 mt-3 text-gray-700 text-sm"></div>
                            </div>
                        </div>
                    </template>
                </div>

            </div>
        </div>

        <div  x-data="app()" class="p-6 rounded-lg bg-white dark:bg-gray-800">
            <div class="md:flex md:justify-between md:items-center">
                <div>
                    <h2 class="text-xl text-gray-800 font-bold leading-tight dark:text-gray-300">Product Sales</h2>
                    <p class="mb-2 text-gray-600 text-sm dark:text-gray-400">Monthly Average</p>
                </div>

             {{-- Legends  --}}
            <div class="mb-4">
                <div class="flex items-center">
                    <div class="w-2 h-2 bgBlue mr-2 rounded-full dark:text-gray-200"></div>
                    <div class="text-sm text-gray-700 dark:text-gray-400">Sales</div>
                </div>
            </div>
            </div>


            <div class="line my-8 relative">
             {{-- Tooltip  --}}
            <template x-if="tooltipOpen == true">
                <div x-ref="tooltipContainer" class="p-0 m-0 z-10 lg rounded-lg absolute h-auto block dark:bg-gray-500" :style="`bottom: ${tooltipY}px; left: ${tooltipX}px`" >
                    <div class="xs rounded-lg bg-white dark:bg-gray-800 p-2">
                        <div class="flex items-center justify-between text-sm">
                            <div class=" dark:text-gray-200">Sales:</div>
                            <div class="font-bold ml-2">
                                <span x-html="tooltipContent" class=" dark:text-gray-200"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </template>

             {{-- Bar Chart  --}}
            <div class="flex -mx-2 items-end mb-2">
                <template x-for="data in chartData">

                    <div class="px-2 w-1/6">
                        <div :style="`height: ${data}px`" class="transition ease-in duration-200 bgBlue hover:bg-blue-400 relative" @mouseenter="showTooltip($event); tooltipOpen = true" @mouseleave="hideTooltip($event)" >
                            <div x-text="data" class="text-center absolute top-0 left-0 right-0 -mt-6 text-gray-800 dark:text-gray-200 text-sm"></div>
                        </div>
                    </div>

                </template>
            </div>

             {{-- Labels  --}}
            <div class="border-t border-gray-400 mx-auto" :style="`height: 1px; width: ${ 100 - 1/chartData.length*100 + 3}%`"></div>
                <div class="flex -mx-2 items-end">
                    <template x-for="data in labels">
                        <div class="px-2 w-1/6">
                            <div class="bg-red-600 relative">
                                <div class="text-center absolute top-0 left-0 right-0 h-2 -mt-px bg-gray-400 mx-auto" style="width: 1px"></div>
                                <div x-text="data" class="text-center absolute top-0 left-0 right-0 mt-3 text-gray-700 text-sm"></div>
                            </div>
                        </div>
                    </template>
                </div>

            </div>
        </div>


    </div>
</div>










<script>
    function app() {
        return {
            chartData: [112, 10, 225, 134, 101, 80, 50, 100, 200],
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],

            tooltipContent: '',
            tooltipOpen: false,
            tooltipX: 0,
            tooltipY: 0,
            showTooltip(e) {
            console.log(e);
            this.tooltipContent = e.target.textContent
            this.tooltipX = e.target.offsetLeft - e.target.clientWidth;
            this.tooltipY = e.target.clientHeight + e.target.clientWidth;
            },
            hideTooltip(e) {
            this.tooltipContent = '';
            this.tooltipOpen = false;
            this.tooltipX = 0;
            this.tooltipY = 0;
            }
        }
    }
</script>
@endsection
