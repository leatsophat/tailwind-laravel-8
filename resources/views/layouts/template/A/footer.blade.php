<script src="{{ asset('plugins/datatable/js/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatable/js/buttons.print.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>
<script src="{{ asset('js/alpine.min.js') }}" defer ></script>
<script src="{{ asset('js/init-alpine.js') }}"></script>
<script src="{{ asset('js/dateTime.demo.js') }}"></script>


{{-- demo testing --}}
<script src="{{ asset('js/demo.js') }}"></script>

