<div class="sticky py-4 text-gray-500 dark:text-gray-400">
    <a class="flex h-9" href="{{ url('/') }}">
        <img src="{{ url('img/logo.png') }}" alt="Logo">
    </a>


    <nav  aria-label="Main" class="flex-1 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto">

        {{-- default --}}
        <div x-data="{ isActive: true, open: false }">
            <a href="#" @click="$event.preventDefault(); open = !open"
                class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                :class="{'bg-primary-100': open}">
                <i class="fl shield-check"></i>
                <span class="ml-2 text-sm"> Default</span>
            </a>
        </div>
        {{-- default --}}



        {{-- sales --}}
        <div x-data="{ isActive: true, open: false}">
            <a href="#" @click="$event.preventDefault(); open = !open"
                class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                :class="{'bg-primary-100 dark:bg-primary': open}"
                role="button" aria-haspopup="true"
                :aria-expanded="(open || isActive) ? 'true' : 'false'"
                aria-expanded="true">

                <i class="fl chart-line"></i>
                <span class="ml-1 text-sm">Sales</span>
                <span class="ml-auto" aria-hidden="true">
                    <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                </span>
            </a>
            <div role="menu" x-show="open" class="m-1 p-1 space-y-2  rounded-md" aria-label="Dashboards">

                {{-- <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">CRM</a> --}}

                {{-- CRM --}}
                <div x-data="{ isActive: true, open: false}">
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700"
                        @click="$event.preventDefault(); open = !open"
                        class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                        :class="{'bg-primary-100 dark:bg-primary': open}"
                        role="button" aria-haspopup="true"
                        :aria-expanded="(open || isActive) ? 'true' : 'false'"
                        aria-expanded="true">
                        <span class="ml-2 text-sm w-4/5">CRM</span>
                        <span class="float-right" aria-hidden="true">
                            <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                        </span>
                    </a>
                    <div role="menu" x-show="open" class="m-1 space-y-2 bg-grar-200 rounded-md" aria-label="Dashboards">

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Dashboard</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Leads</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Opportunities</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Quotations</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Convert</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Report</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Setting</a>
                        </div>


                    </div>
                </div>
                {{-- CRM --}}



                {{-- Sales --}}
                <div x-data="{ isActive: true, open: false}">
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700"
                        @click="$event.preventDefault(); open = !open"
                        class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                        :class="{'bg-primary-100 dark:bg-primary': open}"
                        role="button" aria-haspopup="true"
                        :aria-expanded="(open || isActive) ? 'true' : 'false'"
                        aria-expanded="true">
                        <span class="ml-2 text-sm w-4/5">Sales</span>
                        <span class="float-right" aria-hidden="true">
                            <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                        </span>
                    </a>
                    <div role="menu" x-show="open" class="m-1 p-1 space-y-2 bg-gray-100 rounded-md" aria-label="Dashboards">
                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Quotation</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Customers</a>
                        </div>
                    </div>
                </div>
                {{-- Sales --}}
            </div>
        </div>
        {{-- sales --}}

        {{-- Finance --}}
        <div x-data="{ isActive: true, open: false}">
            <a href="#" @click="$event.preventDefault(); open = !open"
                class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                :class="{'bg-primary-100 dark:bg-primary': open}"
                role="button" aria-haspopup="true"
                :aria-expanded="(open || isActive) ? 'true' : 'false'"
                aria-expanded="true">

                <i class="fl balance-scale"></i>
                <span class="ml-1 text-sm">Finance</span>
                <span class="ml-auto" aria-hidden="true">
                    <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                </span>
            </a>
            <div role="menu" x-show="open" class="m-1 space-y-2  rounded-md" aria-label="Dashboards">

                <a href="#" role="menuitem" class="block ml-3 p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Dashboard</a>

                {{-- CRM --}}
                <div x-data="{ isActive: true, open: false}">
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700"
                        @click="$event.preventDefault(); open = !open"
                        class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                        :class="{'bg-primary-100 dark:bg-primary': open}"
                        role="button" aria-haspopup="true"
                        :aria-expanded="(open || isActive) ? 'true' : 'false'"
                        aria-expanded="true">
                        <span class="ml-3 text-sm w-4/5">Accounting</span>
                        <span class="float-right" aria-hidden="true">
                            <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                        </span>
                    </a>
                    <div role="menu" x-show="open" class="m-1 space-y-2 bg-gray-100 rounded-md" aria-label="Dashboards">

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex mx-3 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Accounts Payable</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex mx-3 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Accounts Receivable</a>
                        </div>

                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex mx-3 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Assets & Revenues</a>
                        </div>


                    </div>
                </div>
                {{-- CRM --}}

                <a href="#" role="menuitem" class="block ml-3 p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Invoicing</a>
                <a href="#" role="menuitem" class="block ml-3 p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Expense</a>
                <a href="#" role="menuitem" class="block ml-3 p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Setting</a>
                <a href="#" role="menuitem" class="block ml-3 p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Reporting</a>



                {{-- Finance --}}
                <div x-data="{ isActive: true, open: false}">
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700"
                        @click="$event.preventDefault(); open = !open"
                        class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                        :class="{'bg-primary-100 dark:bg-primary': open}"
                        role="button" aria-haspopup="true"
                        :aria-expanded="(open || isActive) ? 'true' : 'false'"
                        aria-expanded="true">
                        <span class="ml-2 text-sm w-4/5">Finance</span>
                        <span class="float-right" aria-hidden="true">
                            <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                        </span>
                    </a>
                    <div role="menu" x-show="open" class="m-1 space-y-2  rounded-md" aria-label="Dashboards">
                        <div x-data="{ isActive: true, open: false}">
                            <a  @click="$event.preventDefault(); open = !open"
                                class="flex pl-4 items-center p-2 cursor-pointer text-gray-500 transition-colors rounded-md dark:text-light hover:bg-gray-200 dark:hover:bg-primary bg-primary-100 dark:bg-primary"
                                :class="{'bg-primary-100': open}">Quotation</a>
                        </div>


                    </div>
                </div>
                {{-- Finance --}}
            </div>
        </div>
        {{-- Finance --}}




    </nav>




</div>

