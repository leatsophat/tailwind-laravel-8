<div class="sticky py-4 text-gray-500 dark:text-gray-400">
    <a class="ml-6 text-xl font-bold text-blue-800 dark:text-gray-200 red-to-pink" href="{{ url('/') }}"> SmartERP </a>
    

    <nav  aria-label="Main" class="flex-1 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto">
        
        <div x-data="{ isActive: true, open: true }">
            <a href="#" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="true">
                <i class="fl chart-line"></i>
                <span class="ml-2 text-sm"> Active Class</span>
            </a>
        </div>
        
        
        <div x-data="{ isActive: true, open: false}">
            <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="true">
                <i class="fl chart-line"></i>
                <span class="ml-2 text-sm"> Active Class</span>
                <span class="ml-auto" aria-hidden="true">
                    <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                </span>
            </a>
            <div role="menu" x-show="open" class="mt-2 space-y-2 px-7" aria-label="Dashboards">
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Default </a>
            </div>
        </div>

    </nav>
    
    
    <ul>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="/lineChart">
                <i class="fl chart-line"></i>
            <span class="ml-4">Line Chart</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl search-dollar"></i>
            <span class="ml-4">Finance</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl user-shield"></i>
            <span class="ml-4">Services</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl cube"></i>
            <span class="ml-4">Inventory & MRP</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl users"></i>
            <span class="ml-4">Human Resources</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl copy"></i>
            <span class="ml-4">eDocuments</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="/eApprovals">
                <i class="fl tasks"></i>
            <span class="ml-4">eApprovals</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl file-signature"></i>
            <span class="ml-4">Verify Customer</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl socks"></i>
            <span class="ml-4">Fleet</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl map-marker-alt"></i>
            <span class="ml-4">SmartGPS</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl user-check"></i>
            <span class="ml-4">Set Roles</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            {{-- <span class="absolute inset-y-0 left-0 w-1 bg-red-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span> --}}
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" href="">
                <i class="fl map-marked-alt"></i>
            <span class="ml-4">Drawmap</span>
            </a>
        </li>





        <li class="relative px-6 py-3">

            <button class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200" @click="togglePagesMenu" aria-haspopup="true">
            <span class="inline-flex items-center">
                <i class="fl chart-line"></i>
                <span class="ml-4">Line Chart</span>
            </span>
            <i class="fl chevron-down"></i>
            </button>

            <template x-if="isPagesMenuOpen">
                <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                    <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                        <a class="w-full" href="">CRM</a>
                    </li>
                    <li class="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200">
                        <a class="w-full" href=""> Sales </a>
                    </li>
                </ul>
            </template>

        </li>

    </ul>


    <div class="px-6 my-6">
        <button class="flex items-center justify-between w-full px-4 py-2 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-lg active:bg-blue-600 hover:bg-blue-700 focus:outline-none focus:shadow-outline-blue">
            Create account
            <span class="ml-2" aria-hidden="true">+</span>
        </button>
    </div>

</div>

