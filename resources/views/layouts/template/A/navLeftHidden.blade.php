<div class="py-4 text-gray-500 dark:text-gray-400">
    <a class="ml-6 text-xl font-bold text-gray-800 dark:text-gray-200 red-to-pink" href="#"> SmartERP </a>

    <ul class="mt-6">
        <li class="relative px-6 py-3">
            <span class="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>
            <a class="inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200 dark:text-gray-100" href="/dashboard">
                <t class="fl home"></t>
                <span class="ml-4">Dashboard</span>
            </a>
        </li>
    </ul>

    <ul>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <t class="fl tasks"></t>
                <span class="ml-4">Forms</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl search-dollar"></i>
                <span class="ml-4">Finance</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl user-shield"></i>
            <span class="ml-4">Services</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl cube"></i>
            <span class="ml-4">Inventory & MRP</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl users"></i>
            <span class="ml-4">Human Resources</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl copy"></i>
            <span class="ml-4">eDocuments</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="/eApprovals">
                <i class="fl tasks"></i>
            <span class="ml-4">eApprovals</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl file-signature"></i>
            <span class="ml-4">Verify Customer</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl socks"></i>
            <span class="ml-4">Fleet</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl map-marker-alt"></i>
            <span class="ml-4">SmartGPS</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl user-check"></i>
            <span class="ml-4">Set Roles</span>
            </a>
        </li>

        <li class="relative px-6 py-3">
            <a class="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="">
                <i class="fl map-marked-alt"></i>
            <span class="ml-4">Drawmap</span>
            </a>
        </li>

        <li class="relative px-6 py-3">

            <a class="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" @click="togglePagesMenu" aria-haspopup="true" href="lineChart">
                <span class="inline-flex items-center">
                    <i class="fl chart-line"></i>
                    <span class="ml-4">Line Chart</span>
                </span>
                <svg class="w-4 h-4" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20">
                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                </svg>
            </a>

            <template x-if="isPagesMenuOpen">
                <ul x-transition:enter="transition-all ease-in-out duration-300" x-transition:enter-start="opacity-25 max-h-0" x-transition:enter-end="opacity-100 max-h-xl" x-transition:leave="transition-all ease-in-out duration-300" x-transition:leave-start="opacity-100 max-h-xl" x-transition:leave-end="opacity-0 max-h-0" class="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900" aria-label="submenu">
                    <li class="px-2 py-1 transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200">
                        <a class="w-full" href="">CRM</a>
                    </li>
                    <li class="px-2 py-1 transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200">
                        <a class="w-full" href="">Sales</a>
                    </li>
                </ul>
            </template>

        </li>
    </ul>
    <div class="px-6 my-6">
        <button class="flex items-center justify-between w-full px-4 py-2 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">
            Create account <span class="ml-2" aria-hidden="true">
                <t class="plus"></t></span>
        </button>
    </div>
</div>
