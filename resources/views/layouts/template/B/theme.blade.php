<div x-transition:enter="transition duration-300 ease-in-out" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition duration-300 ease-in-out" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-show="isSettingsPanelOpen" @click="isSettingsPanelOpen = false" class="fixed inset-0 z-10 bg-primary-darker" style="opacity: 0.5" aria-hidden="true"></div>
{{-- Panel  --}}


<section x-transition:enter="transition duration-300 ease-in-out transform sm:duration-500" x-transition:enter-start="translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition duration-300 ease-in-out transform sm:duration-500" x-transition:leave-start="translate-x-0" x-transition:leave-end="translate-x-full" x-ref="settingsPanel" tabindex="-1" x-show="isSettingsPanelOpen" @keydown.escape="isSettingsPanelOpen = false" class="fixed inset-y-0 right-0 z-20 w-full max-w-xs bg-white shadow-xl dark:bg-darker dark:text-light sm:max-w-md focus:outline-none" aria-labelledby="settinsPanelLabel">


    <div class="absolute left-0 p-2 transform -translate-x-full">
        {{-- Close button  --}}
        <button @click="isSettingsPanelOpen = false" class="p-2 text-white rounded-md focus:outline-none focus:ring">
            <i class="fl times"></i>
        </button>
    </div>


    {{-- Panel content  --}}
    <div class="flex flex-col h-screen">



        {{-- Panel header  --}}
        <div class="flex flex-col items-center justify-center flex-shrink-0 px-4 py-8 space-y-4 border-b dark:border-primary-dark">
            <span aria-hidden="true" class="text-gray-500 dark:text-primary">
                <i class="fl cog t-2x"></i>
            </span>
            <h2 id="settinsPanelLabel" class="text-xl font-medium text-gray-500 dark:text-light">Settings</h2>
        </div>


        {{-- Content  --}}
        <div class="flex-1 overflow-hidden hover:overflow-y-auto">
            {{-- Theme  --}}
            <div class="p-4 space-y-4 md:p-8">
                <h6 class="text-lg font-medium text-gray-400 dark:text-light">Mode</h6>
                <div class="flex items-center space-x-8">


                    {{-- Light button  --}}
                    <button @click="setLightTheme" class="flex items-center justify-center px-4 py-2 space-x-4 transition-colors border rounded-md hover:text-gray-900 hover:border-gray-900 dark:border-primary dark:hover:text-primary-100 dark:hover:border-primary-light focus:outline-none focus:ring focus:ring-primary-lighter focus:ring-offset-2 dark:focus:ring-offset-dark dark:focus:ring-primary-dark" :class="{ 'border-gray-900 text-gray-900 dark:border-primary-light dark:text-primary-100': !isDark, 'text-gray-500 dark:text-primary-light': isDark }">
                        <span> <i class="fs star"></i> </span> <span>Light</span>
                    </button>

                    {{-- Dark button  --}}
                    <button @click="setDarkTheme" class="flex items-center justify-center px-4 py-2 space-x-4 transition-colors border rounded-md hover:text-gray-900 hover:border-gray-900 dark:border-primary dark:hover:text-primary-100 dark:hover:border-primary-light focus:outline-none focus:ring focus:ring-primary-lighter focus:ring-offset-2 dark:focus:ring-offset-dark dark:focus:ring-primary-dark" :class="{ 'border-gray-900 text-gray-900 dark:border-primary-light dark:text-primary-100': isDark, 'text-gray-500 dark:text-primary-light': !isDark }">
                        <span> <i class="fs moon"></i> </span> <span>Dark</span>
                    </button>

                </div>
            </div>

             {{-- Colors  --}}
            <div class="p-4 space-y-4 md:p-8">
                <h6 class="text-lg font-medium text-gray-400 dark:text-light">Colors</h6>
                <div>
                    <button @click="setColors('cyan')" class="w-10 h-10 rounded-full"style="background-color: var(--color-cyan)"></button>
                    <button @click="setColors('teal')" class="w-10 h-10 rounded-full"style="background-color: var(--color-teal)"></button>
                    <button @click="setColors('green')" class="w-10 h-10 rounded-full"style="background-color: var(--color-green)"></button>
                    <button @click="setColors('fuchsia')" class="w-10 h-10 rounded-full"style="background-color: var(--color-fuchsia)"></button>
                    <button @click="setColors('blue')" class="w-10 h-10 rounded-full"style="background-color: var(--color-blue)"></button>
                    <button @click="setColors('violet')" class="w-10 h-10 rounded-full"style="background-color: var(--color-violet)"></button>
                </div>
            </div>

        </div>
    </div>
</section>
