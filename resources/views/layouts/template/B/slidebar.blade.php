<aside class="flex-shrink-0 hidden w-64 bg-blue border-r dark:border-primary-darker dark:bg-gray-900 md:block">
    <div class="flex flex-col h-full">

        {{-- Logo  --}}
        <div class="w-full bg-white">
            <a href="" class="w-full">
                <span class="m-auto">
                    <img src="img/logo1.png" width="80%" alt="">
                </span>
            </a>
        </div>
            {{--x Logo  --}}
        {{-- Sidebar links  --}}
        <nav aria-label="Main" class="flex-1 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto">

            {{-- Dashboards  --}}
            <div x-data="{ isActive: true, open: false}">

                {{-- active & hover classes 'bg-primary-100 dark:bg-primary'  --}}
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="true">
                    <span aria-hidden="true">
                        <i class="fl chart-line"></i>
                    </span>
                    <span class="ml-2 text-sm"> Active Class</span>
                    <span class="ml-auto" aria-hidden="true">
                        {{-- active class 'rotate-180'  --}}
                        <i class="fl chevron-down w-4 h-4 transition-transform transform rotate-180" :class="{ 'rotate-180': open }"></i>
                    </span>
                </a>

                <div role="menu" x-show="open" class="mt-2 space-y-2 px-7" aria-label="Dashboards">

                    {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                    {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Default </a>
                </div>

            </div>
            {{--x Dashboards  --}}


            {{-- Components  --}}
            <div x-data="{ isActive: true, open: false}">
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="true">
                    <span aria-hidden="true">
                        <i class="fl layer-group"></i>
                    </span>
                    <span class="ml-2 text-sm"> Componnets </span>
                    <span class="ml-auto" aria-hidden="true">
                        {{-- active class 'rotate-180'  --}}
                        <i class="fl chevron-down w-4 h-4 transition-transform transform" :class="{ 'rotate-180': open }"></i>
                    </span>
                </a>

                <div role="menu" x-show="open" class="mt-2 space-y-2 px-7" aria-label="Components">

                    {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                    {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Dashboard </a>
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Input Forms </a>
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Default </a>
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Default </a>
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Default </a>
                    <a href="" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">Default </a>
                </div>

            </div>
            {{--x Components  --}}


        </nav>

        {{-- Sidebar footer  --}}
        <div class="flex-shrink-0 px-2 py-4 space-y-2">
            <button @click="openSettingsPanel" type="button" class="flex items-center justify-center w-full px-4 py-2 text-sm text-white rounded-md bg-primary hover:bg-primary-dark focus:outline-none focus:ring focus:ring-primary-dark focus:ring-offset-1 focus:ring-offset-white dark:focus:ring-offset-dark">
                <span aria-hidden="true">
                    <i class="fl palette"></i>
                </span>
                <span>Customize</span>
            </button>
        </div>
    </div>
  </aside>
