<header class="relative bg-white dark:bg-darker fixed top-0">

    <div class="flex items-center justify-between p-2 border-b dark:border-primary-darker">
       {{-- Mobile menu button  --}}
        <button @click="isMobileMainMenuOpen = !isMobileMainMenuOpen" class="p-1 transition-colors duration-200 rounded-md text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark md:hidden focus:outline-none focus:ring">
            <span class="sr-only">Open main manu</span>
            <span aria-hidden="true">
                <i class="fl bars"></i>
            </span>
        </button>

        {{-- Brand  --}}
        <a href="" class="inline-block text-2xl font-bold tracking-wider text-primary-dark dark:text-light"> SmartERP</a>

        {{-- Mobile sub menu button  --}}
        <button @click="isMobileSubMenuOpen = !isMobileSubMenuOpen" class="w-8 h-8 p-1 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark md:hidden focus:outline-none focus:ring">
            <span class="sr-only">Open sub manu</span>
            <span aria-hidden="true">
                <i class="fr ellipsis-v"></i>
            </span>
        </button>

        {{-- Desktop Right buttons  --}}
        <nav aria-label="Secondary" class="hidden space-x-2 md:flex md:items-center">
             {{-- Toggle dark theme button  --}}
            <button aria-hidden="true" class="relative focus:outline-none" @click="toggleTheme">
                <div class="w-12 h-6 transition rounded-full outline-none bg-primary-100 dark:bg-primary-lighter"></div>
                <div class="absolute top-0 left-0 inline-flex items-center justify-center w-6 h-6 transition-all duration-150 transform scale-110 rounded-full shadow-sm translate-x-0 -translate-y-px bg-dark text-primary-dark" :class="{ 'translate-x-0 -translate-y-px  bg-white text-primary-dark': !isDark, 'translate-x-6 text-primary-100 bg-primary-darker': isDark }">
                    <i class="fl star" x-show="!isDark"></i>
                    <i class="fl moon" x-show="isDark" style="display: none;"></i>
                </div>
            </button>

             {{-- Notification button  --}}
            <button @click="openNotificationsPanel" class="p-2 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark focus:outline-none focus:bg-primary-100 dark:focus:bg-primary-dark focus:ring-primary-darker">
                <span class="sr-only">Open Notification panel</span>
                <i class="fl bell" aria-hidden="true"></i>
            </button>

             {{-- Search button  --}}
            <button @click="openSearchPanel" class="p-2 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark focus:outline-none focus:bg-primary-100 dark:focus:bg-primary-dark focus:ring-primary-darker">
                <span class="sr-only">Open search panel</span>
                <i class="fl search"></i>
            </button>

             {{-- Settings button  --}}
            <button @click="openSettingsPanel" class="p-2 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark focus:outline-none focus:bg-primary-100 dark:focus:bg-primary-dark focus:ring-primary-darker">
                <span class="sr-only">Open settings panel</span>
                <i class="fl palette" aria-hidden="true"></i>
            </button>

             {{-- User avatar button  --}}
            <div class="relative" x-data="{ open: false }">
                <button @click="open = !open; $nextTick(() => { if(open){ $refs.userMenu.focus() } })" type="button" aria-haspopup="true" :aria-expanded="open ? 'true' : 'false'" class="transition-opacity duration-200 rounded-full dark:opacity-75 dark:hover:opacity-100 focus:outline-none focus:ring dark:focus:opacity-100" aria-expanded="false">
                    <span class="sr-only">User menu</span>
                    <img class="w-10 h-10 rounded-full" src="http://172.17.168.27:82/media/file/main_app/profile/img/4305a275ff1ab1299c159f4693a3ddca.jpg" alt="">
                </button>

                 {{-- User dropdown menu  --}}
                <div x-show="open" x-ref="userMenu" x-transition:enter="transition-all transform ease-out" x-transition:enter-start="translate-y-1/2 opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition-all transform ease-in" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="translate-y-1/2 opacity-0" @click.away="open = false" @keydown.escape="open = false" class="absolute right-0 w-48 py-1 bg-white rounded-md shadow-lg top-12 ring-1 ring-black ring-opacity-5 dark:bg-dark focus:outline-none" tabindex="-1" role="menu" aria-orientation="vertical" aria-label="User menu" style="display: none;">
                    <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary"> Your Profile </a>
                    <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary"> Settings </a>
                    <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary"> Logout </a>
                </div>
            </div>
        </nav>

         {{-- Mobile sub menu  --}}
        <nav x-transition:enter="transition duration-200 ease-in-out transform sm:duration-500" x-transition:enter-start="-translate-y-full opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition duration-300 ease-in-out transform sm:duration-500" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="-translate-y-full opacity-0" x-show="isMobileSubMenuOpen" @click.away="isMobileSubMenuOpen = false" class="absolute flex items-center p-4 bg-white rounded-md shadow-lg dark:bg-darker top-16 inset-x-4 md:hidden z-10" aria-label="Secondary" style="display: none;">
            <div class="space-x-4 flex items-center">
                 {{-- Toggle dark theme button  --}}
                <button aria-hidden="true" class="relative focus:outline-none" @click="toggleTheme"> 
                    <div class="w-12 h-6 transition rounded-full outline-none bg-primary-100 dark:bg-primary-lighter"></div> 
                    <div class="absolute top-0 left-0 inline-flex items-center justify-center w-6 h-6 transition-all duration-200 transform scale-110 rounded-full shadow-sm translate-x-0 -translate-y-px bg-white text-primary-dark" :class="{ 'translate-x-0 -translate-y-px  bg-white text-primary-dark': !isDark, 'translate-x-6 text-primary-100 bg-primary-darker': isDark }"> 
                        {{-- <svg x-show="!isDark" class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">   
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"></path> 
                        </svg>  --}}
                        <i class="fl star w-4 h-4" x-show="!isDark"></i>
                        {{-- <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" style="display: none;">   
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z"></path> 
                        </svg>  --}}
                        <i class="fl moon w-4 h-4" x-show="isDark" style="display: none"></i>
                    </div>
                </button>

                 {{-- Notification button  --}}
                <button @click="openNotificationsPanel(); $nextTick(() => { isMobileSubMenuOpen = false })" class="p-2 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark focus:outline-none focus:bg-primary-100 dark:focus:bg-primary-dark focus:ring-primary-darker">
                    <span class="sr-only">Open notifications panel</span>
                    <i class="fl bell"></i>
                </button>

                 {{-- Search button  --}}
                <button @click="openSearchPanel(); $nextTick(() => { $refs.searchInput.focus(); setTimeout(() => {isMobileSubMenuOpen= false}, 100) })" class="p-2 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark focus:outline-none focus:bg-primary-100 dark:focus:bg-primary-dark focus:ring-primary-darker">
                    <span class="sr-only">Open search panel</span>
                    <i class="fl search"></i>
                </button>

                 {{-- Settings button  --}}
                <button @click="openSettingsPanel(); $nextTick(() => { isMobileSubMenuOpen = false })" class="p-2 transition-colors duration-200 rounded-full text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark focus:outline-none focus:bg-primary-100 dark:focus:bg-primary-dark focus:ring-primary-darker">
                    <span class="sr-only">Open settings panel</span>
                    <i class="fl palette"></i>
                </button>
            </div>

             {{-- User avatar button  --}}
            <div class="relative ml-auto" x-data="{ open: false }">
            <button @click="open = !open" type="button" aria-haspopup="true" :aria-expanded="open ? 'true' : 'false'" class="block transition-opacity duration-200 rounded-full dark:opacity-75 dark:hover:opacity-100 focus:outline-none focus:ring dark:focus:opacity-100" aria-expanded="false">
                <span class="sr-only">User menu</span>
                <img class="w-10 h-10 rounded-full" src="http://172.17.168.27:82/media/file/main_app/profile/img/4305a275ff1ab1299c159f4693a3ddca.jpg" alt="">
            </button>

             {{-- User dropdown menu  --}}
            <div x-show="open" x-transition:enter="transition-all transform ease-out" x-transition:enter-start="translate-y-1/2 opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition-all transform ease-in" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="translate-y-1/2 opacity-0" @click.away="open = false" class="absolute right-0 w-48 py-1 origin-top-right bg-white rounded-md shadow-lg top-12 ring-1 ring-black ring-opacity-5 dark:bg-dark" role="menu" aria-orientation="vertical" aria-label="User menu" style="display: none;">
                <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary"> Your Profile </a>
                <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary"> Settings </a>
                <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary"> Logout </a>
            </div>
            </div>
        </nav>
    </div>



     {{-- Mobile main manu  --}}
    <div class="border-b md:hidden dark:border-primary-darker" x-show="isMobileMainMenuOpen" @click.away="isMobileMainMenuOpen = false" style="display: none;">
        <nav aria-label="Main" class="px-2 py-4 space-y-2">
             {{-- Dashboards links  --}}
            <div x-data="{ isActive: true, open: true}">
                 {{-- active & hover classes 'bg-primary-100 dark:bg-primary'  --}}
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary bg-primary-100 dark:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="true">
                    <span aria-hidden="true">
                        <i class="fl chart-line"></i>
                    </span>
                    <span class="ml-2 text-sm"> Dashboards </span>
                    <span class="ml-auto" aria-hidden="true">
                         {{-- active class 'rotate-180'  --}}
                        <i class="fl angle-down w-5 h-5 transition-transform transform" :class="{ 'rotate-180': open }"></i>
                    </span>
                </a>
                <div role="menu" x-show="open" class="mt-2 space-y-2 px-7" aria-label="Dashboards">
                     {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                     {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                    <a href="index.html" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700"> Default </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Project Mangement (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> E-Commerce (soon) </a>
                </div>
            </div>

             {{-- Components links  --}}
            <div x-data="{ isActive: false, open: false }">
                 {{-- active classes 'bg-primary-100 dark:bg-primary'  --}}
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{ 'bg-primary-100 dark:bg-primary': isActive || open }" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="false">
                    <span aria-hidden="true">
                        <i class="fl columns"></i>
                    </span>
                    <span class="ml-2 text-sm"> Components </span>
                    <span aria-hidden="true" class="ml-auto">
                         {{-- active class 'rotate-180'  --}}
                        <i class="fl angle-down w-5 h-5 transition-transform transform" :class="{ 'rotate-180': open }"></i>
                    </span>
                </a>
                <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" arial-label="Components" style="display: none;">
                     {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                     {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Alerts (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Buttons (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Cards (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Dropdowns (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Forms (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Lists (soon) </a>
                    <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Modals (soon) </a>
                </div>
            </div>

             {{-- Pages links  --}}
            <div x-data="{ isActive: false, open: false }">
             {{-- active classes 'bg-primary-100 dark:bg-primary'  --}}
            <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{ 'bg-primary-100 dark:bg-primary': isActive || open }" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="false">
                <span aria-hidden="true">
                    <i class="fl file-alt"></i>
                </span>
                <span class="ml-2 text-sm"> Pages </span>
                <span aria-hidden="true" class="ml-auto">
                     {{-- active class 'rotate-180'  --}}
                    <i class="fl angle-down w-5 h-5 transition-transform transform" :class="{ 'rotate-180': open }"></i>
                </span>
            </a>

            <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" arial-label="Pages" style="display: none;">
                 {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                 {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Blank </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> 404 </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> 500 </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Profile (soon) </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Pricing (soon) </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Kanban (soon) </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Feed (soon) </a>
            </div>
        </div>

        {{-- Authentication links  --}}
        <div x-data="{ isActive: false, open: false}">
            {{-- active & hover classes 'bg-primary-100 dark:bg-primary'  --}}
            <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="false">
                <span aria-hidden="true">
                    <i class="fl user"></i>
                </span>
                <span class="ml-2 text-sm"> Authentication </span>
                <span aria-hidden="true" class="ml-auto">
                     {{-- active class 'rotate-180'  --}}
                    <i class="fl angle-down w-5 h-5 transition-transform transform" :class="{ 'rotate-180': open }"></i>
                </span>
            </a>
            <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" aria-label="Authentication" style="display: none;">
                 {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                 {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Register </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Login </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Forgot Password </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700"> Reset Password </a>
            </div>
        </div>

        {{-- Layouts links  --}}
        <div x-data="{ isActive: false, open: false}">
            {{-- active & hover classes 'bg-primary-100 dark:bg-primary'  --}}
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'" aria-expanded="false">
                    <span aria-hidden="true">
                        <i class="fl layer-group"></i>
                    </span>
                    <span class="ml-2 text-sm"> Layouts </span>
                    <span aria-hidden="true" class="ml-auto">
                         {{-- active class 'rotate-180'  --}}
                        <i class="fl angle-down w-5 h-5 transition-transform transform" :class="{ 'rotate-180': open }"></i>
                    </span>
                </a>
                <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" aria-label="Layouts" style="display: none;">
                 {{-- active & hover classes 'text-gray-700 dark:text-light'  --}}
                 {{-- inActive classes 'text-gray-400 dark:text-gray-400'  --}}
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Two Columns Sidebar </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Mini + One Columns Sidebar </a>
                <a href="" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700"> Mini Column Sidebar </a>

            </div>
            </div>
        </nav>
    </div>
</header>
