<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="UTF-8">
    {{-- header style --}}
    @include('layouts.template.A.header')
    {{-- End header style --}}
</head>

<body>


<div class="flex h-screen bg-gray-50 dark:bg-gray-900 blue:bg-blue-500" :class="{ 'overflow-hidden': isSideMenuOpen }">

    <aside class="z-20 hidden w-64 overflow-y-auto bg-white dark:bg-gray-800 blue:bg-blue-500 md:block flex-shrink-0">
        @include('layouts.template.A.navLeft') {{--  slidebar left  --}}
    </aside>

    <aside class="fixed inset-y-0 z-20 flex-shrink-0 w-64 mt-16 overflow-y-auto bg-white dark:bg-gray-800 blue:bg-blue-500 md:hidden" x-show="isSideMenuOpen" x-transition:enter="transition ease-in-out duration-150" x-transition:enter-start="opacity-0 transform -translate-x-20" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in-out duration-150" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0 transform -translate-x-20" @click.away="closeSideMenu" @keydown.escape="closeSideMenu" style="display: none;">
        @include('layouts.template.A.navLeftHidden') {{--  Slide bar Left Small Screen  --}}
    </aside>


    <div class="flex flex-col flex-1 w-full">

        <header class="z-10 py-4 bg-white shadow-md dark:bg-gray-800 blue:bg-blue-500">
            @include('layouts.template.A.navHeader') {{--  header navbar  --}}
        </header>


        <main class="h-full overflow-y-auto bg-gray-200 dark:bg-gray-900">
            @yield('content'){{--  content  --}}
        </main>

    </div>


</div>


    @include('layouts.template.A.footer')

    {{-- Footer Script --}}
    @yield('AddMoreScript')
    {{-- End Footer Script --}}


</body>
</html>
