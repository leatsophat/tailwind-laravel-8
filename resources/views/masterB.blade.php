<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>{{ $title }}</title>
        @include('layouts.template.B.header')
    </head>

<body>

<div x-data="setup()" x-init="$refs.loading.classList.add('hidden'); setColors(color);" :class="{ 'dark': isDark}">
    <div class="flex h-screen antialiased text-gray-900 bg-gray-100 dark:bg-dark dark:text-light">
        <div x-ref="loading" class="fixed inset-0 z-50 flex items-center justify-center text-2xl font-semibold text-white bg-primary-darker" > Loading..... </div>

        @include('layouts.template.B.slidebar')

        <div class="flex-1 h-full overflow-x-hidden overflow-y-auto">

            @include('layouts.template.B.navtop')

            <main class="bg-gray-200 dark:bg-dark">
                @yield('content'){{--  content  --}}
            </main>

            @include('layouts.template.B.option')

            @include('layouts.template.B.theme')

        </div>

    </div>
</div>

@include('layouts.template.B.footer')
{{-- Footer Script --}}
@yield('AddMoreScript')
{{-- End Footer Script --}}
</body>
</html>
