<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Choose</title>
    @include('layouts.template.A.header')
</head>

<body class="bg-gray-200">




    <div class="container mt-2 m-auto">
        {{-- Two grid columns  --}}
        <div class="grid grid-cols-1 p-4 space-y-8 lg:gap-4 lg:space-y-0 lg:grid-cols-3">

            {{-- Bar chart card  --}}
            <div class="col-span-3 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200 p-4 text-center">
                <h1 class="text-2xl font-bold text-gray-500 dark:text-light"><i class="fl cog tt-spin red-to-greengray"></i> Demo <span class="blue-to-greengray">SmartERP</span> Admin <i class="fl cog tt-spin red-to-greengray"></i></h1>
            </div>


            {{-- Bar chart card  --}}
            <div class="col-span-1 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200 tranform shadow hover:shadow-xl transition">
                {{-- Card header  --}}
                <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                    <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Template A</h4>
                </div>


                {{-- Chart  --}}
                <div class="relative p-4 h-72">
                    <a href="{{ url('/templateA') }}" class="w-full">
                        <img src="{{ url('img/templateA.png') }}" alt="Template">
                    </a>

                </div>
            </div>

            {{-- Line chart card  --}}
            <div class="col-span-1 bg-white rounded-md dark:bg-gray-800 dark:border-gray-200 tranform shadow hover:shadow-xl transition">
                {{-- Card header  --}}
                <div class="flex items-center justify-between p-4 border-b dark:border-primary">
                    <h4 class="text-lg font-semibold text-gray-500 dark:text-light">Template B</h4>
                </div>

                {{-- Chart  --}}
                <div class="relative p-4 h-72">
                    <a href="{{ url('/templateB') }}" class="w-full">
                        <img src="{{ url('img/templateB.png') }}" alt="Template">
                    </a>

                </div>
            </div>


            
        </div>
    </div>





    <div x-data="{ open: false }">
        <button @click="open = true">Open Dropdown</button>
        <ul x-show="open" @click.away="open = false" >
            Dropdown Body
        </ul>
    </div>


@include('layouts.template.A.footer')

</body>

</html>
